### Steps to run the project:

```bash
-Import the project to Eclipse as existing maven project.
-Run maven clean and then maven install.
-Run the project by running EsgatewayApplication.java as a java application.
-The project uses H2 embedded in-memory data base. 
-You could access the project through this URL http://localhost:8080
-To test the project with Postman you could use this Postman collection ESGateway.postman_collection.json within the project root folder.
```

### For automatic build without IDE check the below steps:
```bash
-Be sure that you installed maven at your machine and is added to your system path variable.
-To build the project directly through maven without using IDE, you could use buld.bat be sure to edit "set JAVA_HOME=" to point to your actual JDK directory.

```

### For running the project without IDE after making the automatic build:
```bash
-To start the application after making the build you could use start.bat file.
```