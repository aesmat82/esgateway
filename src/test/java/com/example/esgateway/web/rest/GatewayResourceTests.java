package com.example.esgateway.web.rest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.example.esgateway.data.dto.DeviceDTO;
import com.example.esgateway.data.dto.GatewayDTO;
import com.example.esgateway.data.enums.DeviceStatus;
import com.example.esgateway.service.GatewayService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class GatewayResourceTests {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private GatewayService gatewayService;
    
    
    ObjectMapper  mapper= new ObjectMapper();



    @Test
    public void test_create_gateway_check_redirect_URL() throws Exception {
        
    	GatewayDTO gatewayDTO = new GatewayDTO(1L,"maingateway","344354","123.123.123.123",null);
    	when(gatewayService.addGateway(gatewayDTO)).thenReturn(gatewayDTO);
    	
        MvcResult result = mvc.perform(MockMvcRequestBuilders
                .post("/api/gateways",gatewayDTO)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(gatewayDTO))
                .accept(MediaType.APPLICATION_JSON))
                .andReturn();

        assertEquals("/api/gateways/1", result.getResponse().getRedirectedUrl());
    }

    
    @Test
    public void test_create_gateway_check_response_status_is_created() throws Exception {
        
    	GatewayDTO gatewayDTO = new GatewayDTO(1L,"maingateway","344354","123.123.123.123",null);
    	when(gatewayService.addGateway(gatewayDTO)).thenReturn(gatewayDTO);
    	
        		mvc.perform(MockMvcRequestBuilders
                .post("/api/gateways",gatewayDTO)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(gatewayDTO))
                .accept(MediaType.APPLICATION_JSON))
        		.andExpect(status().isCreated())
                .andReturn();
        
    }
    
    
    @Test
    public void test_create_gateway_check_response_content() throws Exception {
        
    	GatewayDTO gatewayDTO = new GatewayDTO(1L,"maingateway","344354","123.123.123.123",null);
    	when(gatewayService.addGateway(gatewayDTO)).thenReturn(gatewayDTO);
    	
    	 MvcResult result = mvc.perform(MockMvcRequestBuilders
                .post("/api/gateways",gatewayDTO)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(gatewayDTO))
                .accept(MediaType.APPLICATION_JSON))
        		.andExpect(status().isCreated())
                .andReturn();
        
        		 assertEquals(mapper.writeValueAsString(gatewayDTO), result.getResponse().getContentAsString());
    }
    
    
  
    
	  @Test
	    public void test_create_gateway_check_exceed_max_no_of_pripherals() throws Exception {
	        
	    	GatewayDTO gatewayDTO = new GatewayDTO(1L,"maingateway","344354","123.123.123.123",null);
	    	
	    	  List<DeviceDTO> devices=new ArrayList<DeviceDTO>();
	    		
	    		for (long i = 0; i < 13; i++) {
	    			devices.add(new DeviceDTO(i, "", null, DeviceStatus.onstatus, gatewayDTO.getId()) );
	    		}
	    		
	    		gatewayDTO.setPeripheralDevices(devices);
	    	
	    	when(gatewayService.addGateway(gatewayDTO)).thenReturn(gatewayDTO);
	    	
	    			mvc.perform(MockMvcRequestBuilders
	                .post("/api/gateways",gatewayDTO)
	                .contentType(MediaType.APPLICATION_JSON_VALUE)
	                .content(mapper.writeValueAsString(gatewayDTO))
	                .accept(MediaType.APPLICATION_JSON))
	        		.andExpect(status().isBadRequest())
	                .andReturn();
	        
	    }
	  
    @Test
    public void test_create_gateway_check_ivalid_IP4_format() throws Exception {
        
    	GatewayDTO gatewayDTO = new GatewayDTO(1L,"maingateway","344354","bad_IP4_12355.123555123",null);
    	when(gatewayService.addGateway(gatewayDTO)).thenReturn(gatewayDTO);
    	
    	 mvc.perform(MockMvcRequestBuilders
                .post("/api/gateways",gatewayDTO)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(gatewayDTO))
                .accept(MediaType.APPLICATION_JSON))
        		.andExpect(status().isBadRequest())
                .andReturn();
        
    }
    
    
    @Test
    public void test_create_gateway_check_empty_serial_no() throws Exception {
        
    	GatewayDTO gatewayDTO = new GatewayDTO(1L,"maingateway","","123.123.123.123",null);
    	when(gatewayService.addGateway(gatewayDTO)).thenReturn(gatewayDTO);
    	
    	 mvc.perform(MockMvcRequestBuilders
                .post("/api/gateways",gatewayDTO)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(gatewayDTO))
                .accept(MediaType.APPLICATION_JSON))
        		.andExpect(status().isBadRequest())
                .andReturn();
        
    }
    
    
    @Test
    public void test_findAllGateways_response_content() throws Exception {
        
    	GatewayDTO gatewayDTO = new GatewayDTO(1L,"maingateway","344354","123.123.123.123",null);
    	List<GatewayDTO> gateways = new ArrayList<GatewayDTO>();
    	gateways.add(gatewayDTO);
    	
    	
    	when(gatewayService.findAllGatways(ArgumentMatchers.any())).thenReturn(gateways);
    	
    	 mvc.perform(MockMvcRequestBuilders
                .get("/api/gateways?page=1,size=10")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                 .andExpect(MockMvcResultMatchers.jsonPath("$.[0]").exists())
                .andReturn();
        
    }
    
    
    @Test
    public void test_findAll_findGatewayById_success() throws Exception {
        
    	Long gatewayId = 1L;
    	GatewayDTO gatewayDTO = new GatewayDTO(1L,"maingateway","344354","123.123.123.123",null);
    	
    	when(gatewayService.findByGatewayId(gatewayId)).thenReturn(gatewayDTO);
    	
    	MvcResult result =  mvc.perform(MockMvcRequestBuilders
                .get("/api/gateways/"+gatewayId)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn();
        
    	
    	assertEquals(mapper.writeValueAsString(gatewayDTO), result.getResponse().getContentAsString());
    }
}