package com.example.esgateway.web.rest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.example.esgateway.data.dto.DeviceDTO;
import com.example.esgateway.data.enums.DeviceStatus;
import com.example.esgateway.service.DeviceService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DeviceResourceTests {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private DeviceService deviceService;
    
    ObjectMapper  mapper= new ObjectMapper();



    @Test
    public void test_createDevice_check_redirect_URL() throws Exception {
        
    	DeviceDTO deviceDTO = new DeviceDTO(1L,"Sisco",null,DeviceStatus.onstatus,2L);
    	when(deviceService.addDevice(deviceDTO)).thenReturn(deviceDTO);
    	
        MvcResult result = mvc.perform(MockMvcRequestBuilders
                .post("/api/devices",deviceDTO)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(deviceDTO))
                .accept(MediaType.APPLICATION_JSON))
                .andReturn();

        assertEquals("/api/devices/1", result.getResponse().getRedirectedUrl());
    }

    @Test
    public void test_createDevice_check_response_status() throws Exception {
        
    	DeviceDTO deviceDTO = new DeviceDTO(1L,"Sisco",null,DeviceStatus.onstatus,2L);
    	when(deviceService.addDevice(deviceDTO)).thenReturn(deviceDTO);
    	
        mvc.perform(MockMvcRequestBuilders
                .post("/api/devices",deviceDTO)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(deviceDTO))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

    }
    
    
    @Test
    public void test_createDevice_check_response_content() throws Exception {
        
    	DeviceDTO deviceDTO = new DeviceDTO(1L,"Sisco",null,DeviceStatus.onstatus,2L);
    	when(deviceService.addDevice(deviceDTO)).thenReturn(deviceDTO);
    	
        MvcResult result = mvc.perform(MockMvcRequestBuilders
                .post("/api/devices",deviceDTO)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(deviceDTO))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        assertEquals(mapper.writeValueAsString(deviceDTO), result.getResponse().getContentAsString());
    }
    
    @Test
    public void test_delete_device_response_content() throws Exception {
        
    	Long deviceUID =1L;
    	
        MvcResult result = mvc.perform(MockMvcRequestBuilders
                .delete("/api/devices/"+deviceUID)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        assertEquals("device with uid:"+deviceUID+" was deleted!", result.getResponse().getContentAsString());
    }

    
    @Test
    public void test_findAllDevices_response_content() throws Exception {
        
    	DeviceDTO deviceDTO = new DeviceDTO(1L,"Sisco",null,DeviceStatus.onstatus,2L);
    	
    	List<DeviceDTO> devices = new ArrayList<DeviceDTO>();
    	devices.add(deviceDTO);
    	
    	
    	when(deviceService.findAllDevices(ArgumentMatchers.any())).thenReturn(devices);
    	
    	 mvc.perform(MockMvcRequestBuilders
                .get("/api/devices?page=1,size=10")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                 .andExpect(MockMvcResultMatchers.jsonPath("$.[0]").exists())
                .andReturn();
        
    }
}