package com.example.esgateway.service.impl;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import com.example.esgateway.data.dto.DeviceDTO;
import com.example.esgateway.data.enums.DeviceStatus;
import com.example.esgateway.entity.Device;
import com.example.esgateway.errors.BadRequestAlertException;
import com.example.esgateway.errors.IdAlreadyExists;
import com.example.esgateway.repository.DeviceRepository;
import com.example.esgateway.service.DeviceService;
import com.example.esgateway.service.GatewayService;

@RunWith(MockitoJUnitRunner.class)
public class DeviceServiceImplTests {

	
    DeviceService  deviceService ;
    GatewayService gatewayService;
	
    ModelMapper modelMapper;

    DeviceRepository deviceRepository;
    
    
    @Before
	public void initObj() {
    	deviceRepository = Mockito.mock(DeviceRepository.class);
    	gatewayService = Mockito.mock(GatewayServiceImpl.class);
    	modelMapper=new ModelMapper();
    	
    	deviceService  = new DeviceServiceImpl(deviceRepository, modelMapper,gatewayService);
		
	}
    
    @Test(expected = BadRequestAlertException.class)
    public void test_addDevice_withoutParentGateway() {

    	deviceService.addDevice(new DeviceDTO());

    }
    
    
    @Test(expected = BadRequestAlertException.class)
    public void test_addDevice_with_invalid_parent_Gateway() {

    	DeviceDTO deviceDTO =	new DeviceDTO();
    	deviceDTO.setGatewayId(1L);
    	
    	 when(gatewayService.isValidateGatewy(deviceDTO.getGatewayId())).thenReturn(false);
    	 
    	deviceService.addDevice(deviceDTO);

    }
    
    @Test(expected = IdAlreadyExists.class)
    public void test_addDevice_with_already_exists_UID() {

    	DeviceDTO deviceDTO =	new DeviceDTO();
    	deviceDTO.setGatewayId(1L);
    	
    	 when(gatewayService.isValidateGatewy(deviceDTO.getGatewayId())).thenReturn(true);
    	 when(deviceRepository.findById(deviceDTO.getUid())).thenReturn(Optional.of(new Device()));
    	 
    	deviceService.addDevice(deviceDTO);

    }
    
    @Test
    public void test_addDevice_Successfully() {

    	DeviceDTO deviceDTO =	new DeviceDTO();
    	deviceDTO.setGatewayId(1L);
    	deviceDTO.setUid(123L);
    	
    	 when(gatewayService.isValidateGatewy(deviceDTO.getGatewayId())).thenReturn(true);
    	 
    	 when( deviceRepository.save(modelMapper.map(deviceDTO, Device.class))).thenReturn(modelMapper.map(deviceDTO, Device.class));
    	
    	 assertEquals(deviceDTO, deviceService.addDevice(deviceDTO));

    }
    
    @Test
    public void test_delete_device_Successfully() {

    	Long uid=123L;
    	    	
    	 assertTrue(deviceService.deleteDevice(uid));

    }
    
    
    @Test
    public void test_findAllDevices() {
    	
    	ArrayList<Device> devices=new ArrayList<Device>();
    	devices.add(new Device(22L,"sisco",null,DeviceStatus.onstatus,1L));
    	
    	Page<Device> page=new PageImpl<Device>(devices);
    	
    	 when(deviceRepository.findAll(PageRequest.of(1, 10))).thenReturn(page);
    	 
    	 List<DeviceDTO> expectedResult = modelMapper.map(devices,new TypeToken<List<DeviceDTO>>() {}.getType());
    	 
    	 
    	 assertEquals(expectedResult, deviceService.findAllDevices(PageRequest.of(1, 10)));
    	
    }
}

