package com.example.esgateway.service.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import com.example.esgateway.data.dto.GatewayDTO;
import com.example.esgateway.entity.Device;
import com.example.esgateway.entity.Gateway;
import com.example.esgateway.errors.IdAlreadyExists;
import com.example.esgateway.repository.GatewayRepository;

@RunWith(MockitoJUnitRunner.class)
public class GatewayServiceImplTests {

	
    GatewayServiceImpl gatewayServiceImpl;
	
	
    ModelMapper modelMapper;

    GatewayRepository gatewayRepository;
    
    
    @Before
	public void initObj() {
    	gatewayRepository= Mockito.mock(GatewayRepository.class);
    	modelMapper=new ModelMapper();
    	
    	gatewayServiceImpl = new GatewayServiceImpl(gatewayRepository, modelMapper);
		
	}
    
    @Test(expected = IllegalArgumentException.class)
    public void addGateway_with_null_serial_no() {

    	GatewayDTO gatewayDTO = new GatewayDTO(null, null ,null, null, null);
    	gatewayServiceImpl.addGateway(gatewayDTO);

    }
    
    @Test(expected = IdAlreadyExists.class)
    public void addGateway_with_exists_serial_no() {


    	GatewayDTO gatewayDTO = new GatewayDTO(null,"gatewayName", "123", null, null);
    	
    	 when(gatewayRepository.findBySerialNo(gatewayDTO.getSerialNo())).thenReturn(Optional.of(new Gateway()));

    	 gatewayServiceImpl.addGateway(gatewayDTO);

    }
    
    
    @Test
    public void addGateway_successfully() {


    	GatewayDTO gatewayDTO = new GatewayDTO(null,"gatewayName", "123", null, null);
    	
    	Gateway gatewayEntityToBeSaved=modelMapper.map(gatewayDTO,Gateway.class);
    	 when(gatewayRepository.save(gatewayEntityToBeSaved)).thenReturn(gatewayEntityToBeSaved);

    	GatewayDTO addedGatewayDTO = gatewayServiceImpl.addGateway(gatewayDTO);
    	
    	assertEquals(addedGatewayDTO.getSerialNo(), gatewayDTO.getSerialNo());

    }
    
    @Test
    public void test_findAllGatways() {
    	
    	ArrayList<Gateway> gateways=new ArrayList<Gateway>();
    	gateways.add(new Gateway(1L,"name","serialNo","ip4",null));
    	
    	Page<Gateway> page=new PageImpl<Gateway>(gateways);
    	
    	 when(gatewayRepository.findAll(PageRequest.of(1, 10))).thenReturn(page);
    	 
    	 List<GatewayDTO> expectedResult = modelMapper.map(gateways,new TypeToken<List<GatewayDTO>>() {}.getType());
    	 
    	 
    	 assertEquals(expectedResult, gatewayServiceImpl.findAllGatways(PageRequest.of(1, 10)));
    	
    }

    
    @Test
    public void test_findByGatewayId_with_found_gateway() {
    	
    	ArrayList<Gateway> gateways=new ArrayList<Gateway>();
    	gateways.add(new Gateway(1L,"name","serialNo","ip4",null));
    	
    	 Long gatewayId=1L;
    	 
		when(gatewayRepository.findById(gatewayId)).thenReturn(Optional.of(new Gateway(1L, null, null, null, null)));
    	 
    	 assertEquals(gatewayId, gatewayServiceImpl.findByGatewayId(gatewayId).getId());
    	
    }
    
    
    @Test
    public void test_findByGatewayId_with_not_found_gateway() {
    	
    	
    	 Long gatewayId=1L;
    	 
		when(gatewayRepository.findById(gatewayId)).thenReturn(Optional.of(new Gateway()));
    	 
    	 assertNull(gatewayServiceImpl.findByGatewayId(gatewayId).getId());
    	
    }
    
    
    @Test
    public void test_isValidateGatewy_with_exists_gateway_and_pripheral_less_9() {
    	
    	Long gatewayId=1L;
    	when(gatewayRepository.findById(gatewayId)).thenReturn(Optional.of(new Gateway(1L, null, null, null, new ArrayList<Device>())));
    	
    	assertTrue(gatewayServiceImpl.isValidateGatewy(gatewayId));
    	
    }
    
    @Test
    public void test_isValidateGatewy_with_not_exists_gateway() {
    	
    	Long gatewayId=1L;
    	when(gatewayRepository.findById(gatewayId)).thenReturn(Optional.empty());
    	
    	assertFalse(gatewayServiceImpl.isValidateGatewy(gatewayId));
    	
    }
    
    @Test
    public void test_isValidateGatewy_with_exists_gateway_with_pripherals_more_9() {
    	
    	Long gatewayId=1L;
    	
    	ArrayList<Device> devices =new ArrayList<Device>();
    	for (int i = 0; i < 12; i++) {
    		devices.add(new Device());
		}
    	
    	when(gatewayRepository.findById(gatewayId)).thenReturn(Optional.of(new Gateway(1L, null, null, null, devices)));
    	
    	assertFalse(gatewayServiceImpl.isValidateGatewy(gatewayId));
    	
    }
}

