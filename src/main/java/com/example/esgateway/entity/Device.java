package com.example.esgateway.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.example.esgateway.data.enums.DeviceStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Entity
@Table(name = "device")
@NoArgsConstructor
@AllArgsConstructor
public class Device {
	
	
	@Id
	private Long uid;
	private String vendor;
	@Column(name = "created_date")
	private Date createdDate;
	private DeviceStatus status;
	@Column(name = "gateway_id")
	private Long gatewayId;
}
