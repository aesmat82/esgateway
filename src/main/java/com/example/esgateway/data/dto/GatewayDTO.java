package com.example.esgateway.data.dto;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class GatewayDTO {
	
	
	private Long id;
	
	@Pattern(regexp = "^((?!</script>).)*$", message = "name may have unsafe value!")
	private String name;
	
	@NotBlank(message ="serialNo must not have value!" )
	@Pattern(regexp = "^((?!</script>).)*$", message = "serialNo may have unsafe value!")
	private String serialNo;
	
	@Pattern(regexp = "^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$")
	private String ip4;
	
	@Size(max=10)
	private List<DeviceDTO> peripheralDevices;

}
