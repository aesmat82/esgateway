package com.example.esgateway.data.dto;

import java.util.Date;

import javax.validation.constraints.Pattern;

import com.example.esgateway.data.enums.DeviceStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class DeviceDTO {

	
	private Long uid;
	@Pattern(regexp = "^((?!</script>).)*$", message = "vendor may have unsafe value!")
	private String vendor;
	private Date createdDate;
	private DeviceStatus status;
	private Long gatewayId;
	
}
