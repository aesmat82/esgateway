package com.example.esgateway.data.error;

import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Data
@Builder
public class ApiError
{

    private String errorMessage;
    private Map<String, String> detailedErrors;

    public ApiError(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public ApiError(String message, Map<String, String> detailedErrors)
    {
        this.errorMessage = message;
        this.detailedErrors = detailedErrors;
    }
}
