package com.example.esgateway.data.error;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ApiErrorWrapper
{
    private final ApiError apiError;
}
