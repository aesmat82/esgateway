package com.example.esgateway.data.enums;

public enum DeviceStatus {
	
	onstatus(0),
	ofstatus(0);
	
	private int status;
	
	DeviceStatus(int status){
		this.status=status;
	}

	public int getStatus() {
		return status;
	}

}
