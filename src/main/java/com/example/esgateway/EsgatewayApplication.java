package com.example.esgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsgatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(EsgatewayApplication.class, args);
	}

}
