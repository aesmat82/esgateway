package com.example.esgateway.errors.handler;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.esgateway.data.error.ApiError;
import com.example.esgateway.data.error.ApiErrorWrapper;
import com.example.esgateway.errors.BadRequestAlertException;
import com.example.esgateway.utils.Constant;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice(annotations = RestController.class)
@Slf4j
public class ExceptionHandlingController extends ResponseEntityExceptionHandler
{

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException e, HttpHeaders headers, HttpStatus status, WebRequest request)
    {
        log.error("MethodArgumentNotValidException Error: {}", Constant.GatwayError.INVALID_REQUEST_INPUTS);
        return new ResponseEntity<>(ApiErrorWrapper.builder().apiError(ApiError.builder().errorMessage(Constant.GatwayError.INVALID_REQUEST_INPUTS).detailedErrors(getErrorsMap(e.getBindingResult())).build()).build(), HttpStatus.BAD_REQUEST);
    }



    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ApiErrorWrapper> handleIllegalArgumentException(final IllegalArgumentException e)
    {
        log.error("IllegalArgumentException Error: {}", e.getMessage());
        return new ResponseEntity<>(ApiErrorWrapper.builder().apiError(ApiError.builder().errorMessage(e.getMessage()).build()).build(), HttpStatus.BAD_REQUEST);
    }

    
    @ExceptionHandler(EmptyResultDataAccessException.class)
    public ResponseEntity<ApiErrorWrapper> handleEmptyResultDataAccessException(final EmptyResultDataAccessException e)
    {
        log.error("EmptyResultDataAccessException Error: {}", e.getMessage());
        return new ResponseEntity<>(ApiErrorWrapper.builder().apiError(ApiError.builder().errorMessage(e.getMessage()).build()).build(), HttpStatus.NOT_FOUND);
    }
    
    
    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<ApiErrorWrapper> handleNoSuchElementException(final NoSuchElementException e)
    {
        log.error("NoSuchElementException Error: {}", e.getMessage());
        return new ResponseEntity<>(ApiErrorWrapper.builder().apiError(ApiError.builder().errorMessage(e.getMessage()).build()).build(), HttpStatus.NOT_FOUND);
    }
    
    @ExceptionHandler(BadRequestAlertException.class)
    public ResponseEntity<ApiErrorWrapper> handleBadRequestAlertException(final BadRequestAlertException e)
    {
        log.error("BadRequestAlertException Error: {}", e.getMessage());
        return new ResponseEntity<>(ApiErrorWrapper.builder().apiError(ApiError.builder().errorMessage(e.getMessage()).build()).build(), HttpStatus.BAD_REQUEST);
    }
    
  
    protected Map<String, String> getErrorsMap(BindingResult bindingResult)
    {
        Map<String, String> errorsMap = new HashMap<>();
        List<FieldError> allErrors = bindingResult.getFieldErrors();
        for (FieldError err : allErrors)
        {
            errorsMap.put(err.getField(), err.getDefaultMessage());
        }
        return errorsMap;
    }

}
