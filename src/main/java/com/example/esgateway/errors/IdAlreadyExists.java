package com.example.esgateway.errors;

public class IdAlreadyExists extends BadRequestAlertException {

    private static final long serialVersionUID = 1L;

    public IdAlreadyExists(String defaultMessage, String entityName, String errorKey) {
        super( defaultMessage, entityName,errorKey);
    }
}
