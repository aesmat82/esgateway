package com.example.esgateway.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.example.esgateway.data.dto.GatewayDTO;

public interface GatewayService {

	public GatewayDTO addGateway(GatewayDTO gateway) throws com.example.esgateway.errors.BadRequestAlertException;
	
	public List<GatewayDTO> findAllGatways(Pageable pageable) throws com.example.esgateway.errors.BadRequestAlertException;
	
	public GatewayDTO findByGatewayId(Long gatewayId) throws com.example.esgateway.errors.BadRequestAlertException;

	public boolean isValidateGatewy(Long gatewayId);
	
}
