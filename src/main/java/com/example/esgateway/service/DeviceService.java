package com.example.esgateway.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.example.esgateway.data.dto.DeviceDTO;
import com.example.esgateway.errors.BadRequestAlertException;

public interface DeviceService {

	public DeviceDTO addDevice(DeviceDTO device) throws BadRequestAlertException;
	public boolean deleteDevice(Long uid) throws BadRequestAlertException;
	public List<DeviceDTO> findAllDevices(Pageable pageable);
}
