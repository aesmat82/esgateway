package com.example.esgateway.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.esgateway.data.dto.GatewayDTO;
import com.example.esgateway.entity.Device;
import com.example.esgateway.entity.Gateway;
import com.example.esgateway.errors.BadRequestAlertException;
import com.example.esgateway.errors.IdAlreadyExists;
import com.example.esgateway.repository.GatewayRepository;
import com.example.esgateway.service.GatewayService;

@Service
public class GatewayServiceImpl implements GatewayService {

	
	private GatewayRepository gatewayRepository;
	
	ModelMapper modelMapper;
	
	public GatewayServiceImpl(GatewayRepository gatewayRepository,ModelMapper modelMapper) {
		super();
		this.gatewayRepository = gatewayRepository;
		this.modelMapper = modelMapper;
	}



	@Override
	public GatewayDTO addGateway(GatewayDTO gateway) throws BadRequestAlertException {
		
		
		if (gatewayRepository.findBySerialNo(gateway.getSerialNo()).isPresent()) {
            throw new IdAlreadyExists("Gateway with serial no. "+gateway.getSerialNo()+" is already exists","GatewayManagement","400");
        } 
		
		
		Gateway gatewayentity = modelMapper.map(gateway, Gateway.class);
		
		return modelMapper.map(gatewayRepository.save(gatewayentity),GatewayDTO.class);
	}



	@Override
	public List<GatewayDTO> findAllGatways(Pageable pageable) throws BadRequestAlertException {
		
		List<Gateway>  gateways= gatewayRepository.findAll(pageable).getContent();
		
		return modelMapper.map(gateways,new TypeToken<List<GatewayDTO>>() {}.getType());
	}



	@Override
	public GatewayDTO findByGatewayId(Long gatewayId) throws NoSuchElementException {
		
		Gateway  gateway =gatewayRepository.findById(gatewayId).get();
		
		return modelMapper.map(gateway,GatewayDTO.class);
	}



	@Override
	public boolean isValidateGatewy(Long gatewayId) {

		Gateway gateway =gatewayRepository.findById(gatewayId).orElse(null);
		
		if(gateway==null || !(Optional.of(gateway.getPeripheralDevices()).orElse(new ArrayList<Device>()).size()<9) ) {
			return false;
		}
		
		return true;
	}



}
