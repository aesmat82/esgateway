package com.example.esgateway.service.impl;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.esgateway.data.dto.DeviceDTO;
import com.example.esgateway.entity.Device;
import com.example.esgateway.errors.BadRequestAlertException;
import com.example.esgateway.errors.IdAlreadyExists;
import com.example.esgateway.repository.DeviceRepository;
import com.example.esgateway.service.DeviceService;
import com.example.esgateway.service.GatewayService;

@Service
public class DeviceServiceImpl implements DeviceService {

	
	private DeviceRepository deviceRepository;
	
	private GatewayService gatewayService;
	
	ModelMapper modelMapper;
	
	public DeviceServiceImpl(DeviceRepository deviceRepository,ModelMapper modelMapper,GatewayService gatewayService) {
		super();
		this.deviceRepository = deviceRepository;
		this.modelMapper = modelMapper;
		this.gatewayService=gatewayService;
	}



	@Override
	public DeviceDTO addDevice(DeviceDTO device) throws BadRequestAlertException {
		
		
		if (device.getGatewayId()==null) {
            throw new BadRequestAlertException("parent gateway can't be empty", "DeviceManagement", "400");
        } else if(!gatewayService.isValidateGatewy(device.getGatewayId())){
        	throw new BadRequestAlertException("not valid parent gateway", "DeviceMnagement", "400");
        } else if (deviceRepository.findById(device.getUid()).isPresent()) {
            throw new IdAlreadyExists("Device with UID "+device.getUid()+" is already exists","DeviceMnagement","400");
        } 
		
		
		Device savedDeviceEntity = deviceRepository.save(modelMapper.map(device, Device.class));
		
				
		return modelMapper.map(savedDeviceEntity,DeviceDTO.class);
	}



	@Override
	public boolean deleteDevice(Long uid) throws EmptyResultDataAccessException {
		deviceRepository.deleteById(uid);
		return true;
	}



	@Override
	public List<DeviceDTO> findAllDevices(Pageable pageable) {
		List<Device> devices = deviceRepository.findAll(pageable).getContent();
		
		return modelMapper.map(devices,new TypeToken<List<DeviceDTO>>() {}.getType());
	}

}
