package com.example.esgateway.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeansConfiguration {

	
	@Bean("modelMapper")
    public ModelMapper createModelMapper()
    {
		ModelMapper modelMapper = new ModelMapper();
		
		return modelMapper;
    }
}
