package com.example.esgateway.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.esgateway.data.dto.DeviceDTO;
import com.example.esgateway.errors.BadRequestAlertException;
import com.example.esgateway.service.DeviceService;

@RestController
@RequestMapping("/api")
public class DeviceResource {
	
	  private final Logger log = LoggerFactory.getLogger(DeviceResource.class);
	
	@Autowired
	DeviceService deviceService;
	
	@PostMapping("/devices")
    public ResponseEntity<DeviceDTO> createDevice(@Valid @RequestBody DeviceDTO deviceDTO) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to createDevice : {}", deviceDTO);

        {
        	DeviceDTO savedDevice = deviceService.addDevice(deviceDTO);
            return ResponseEntity.created(new URI("/api/devices/" + savedDevice.getUid()))
                .body(savedDevice);
        }
    }
	
	
	@DeleteMapping("/devices/{uid}")
    public ResponseEntity<String> createDevice(@Valid @PathVariable Long uid) throws URISyntaxException, BadRequestAlertException {
		
		log.debug("REST request to delete device uid : {}", uid);
		
		deviceService.deleteDevice(uid);
		
		log.debug("device with uid was deleted : {}", uid);
		
		return ResponseEntity.ok("device with uid:"+uid+" was deleted!");
		
	}
       
	@GetMapping("/devices")
    public ResponseEntity<List<DeviceDTO>> findAllDevices(Pageable pageable) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to findAllDevices");

        {
        	List<DeviceDTO> gateway = deviceService.findAllDevices(pageable);
            return  new ResponseEntity<List<DeviceDTO>>(gateway,  HttpStatus.OK);
        }
    }
	

}
