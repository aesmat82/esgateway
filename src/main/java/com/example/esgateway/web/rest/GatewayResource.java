package com.example.esgateway.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.esgateway.data.dto.GatewayDTO;
import com.example.esgateway.errors.BadRequestAlertException;
import com.example.esgateway.service.GatewayService;

@RestController
@RequestMapping("/api")
public class GatewayResource {
	
	  private final Logger log = LoggerFactory.getLogger(GatewayResource.class);
	
	@Autowired
	GatewayService gatewayService;
	
	@PostMapping("/gateways")
    public ResponseEntity<GatewayDTO> createGateway(@Validated @RequestBody GatewayDTO gatwyaDTO) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to save User : {}", gatwyaDTO);

        {
            GatewayDTO gateway = gatewayService.addGateway(gatwyaDTO);
            return ResponseEntity.created(new URI("/api/gateways/" + gateway.getId()))
                .body(gateway);
        }
    }
	
	
	@GetMapping("/gateways")
    public ResponseEntity<List<GatewayDTO>> findAllGateways(Pageable pageable) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to findAllGateways");

        {
        	List<GatewayDTO> gateway = gatewayService.findAllGatways(pageable);
            return  new ResponseEntity<List<GatewayDTO>>(gateway,  HttpStatus.OK);
        }
    }
	
	@GetMapping("/gateways/{gatewayId}")
    public ResponseEntity<GatewayDTO> findGatewayById(@PathVariable Long gatewayId) throws URISyntaxException, BadRequestAlertException {
        log.debug("REST request to findGatewayById");

        {
        	GatewayDTO gateway = gatewayService.findByGatewayId(gatewayId);
            return  new ResponseEntity<GatewayDTO>(gateway,  HttpStatus.OK);
        }
    }

}
