package com.example.esgateway.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.esgateway.entity.Device;

public interface DeviceRepository  extends JpaRepository<Device, Long>{
	
	
	public List<Device> findByGatewayId (Long gatewayId);

}
