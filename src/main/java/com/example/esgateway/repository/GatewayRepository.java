package com.example.esgateway.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.esgateway.entity.Gateway;

public interface GatewayRepository  extends JpaRepository<Gateway, Long>{
	
	
	public Optional<Gateway> findBySerialNo (String serialNo);
	
	public Optional<Gateway> findById (Long gatewayId);

}
